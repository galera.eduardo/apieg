const connections = require("../../db/db.config");
const Assert = require("assert");
const {
  getAllUsers,
  getUserById,
  createNewUsers,
  updateUser,
  deleteUser,
  login,
  changePasswordById
} =require("../user/userModelos")


const assert = Assert.strict;

console.log(assert)
describe("Testing de modelos de Usuarios", function(){
    
  it("El modelo debe devolver usuarios en formato array",
 async function(){
        const result = await getAllUsers();
        assert.strictEqual(Array.isArray(result, true))
         });

      it("Debe insertar un usuario correctamente", 
      async function(){
        const Users ={
          "name": "Copito",
          "email": "copito@gmail.com",
          "password": "Copito-1234",
          "image": "image.jpg"
          }
        const result = await createNewUsers(Users);
        assert.ok(result.insertId)
      });

      it("Debo obtener un usuario por id", 
    async function(){
      const result = await getUserById(1);
      assert.ok(result[0].id)
    })
})