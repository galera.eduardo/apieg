const { check, validationResult } = require("express-validator");

const validatorResetPassword = [
  check("password_1")
    .exists()
    .notEmpty()
    .withMessage("El password debe existir")
    .isLength({ min: 8, max: 25 })
    .withMessage("min:8 , max:25")
    .matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/)
    .withMessage("Password Debe contener un formato correcto"),
 check("password_2")
    .custom(async(password_2, {req})=>{
        const password_1 = req.body.password_1;
        if(password_1 !== password_2){
            throw new Error("Password must be identical")
        }
    }),
    (req, res, next)=>{
        const token = req.params.token
        const error = validationResult(req)
        if(!error .isEmpty()){
            const arrWarnings = error.array()
            res.render("reset", {arrWarnings, token})
        }
    }
];

module.exports = validatorResetPassword