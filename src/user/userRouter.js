const router = require("express").Router();
const {
  allUsers,
  userById,
  upUser,
  userDelete,
   } = require("./userControllers");
 const {loginUser, 
        register, 
        reset, 
        saveNewPassword, 
        forgot
      } = require("./registerLoginResetController")
const fileUpload = require("../util/handleStorage");
const validatorCreateUser = require("../validators/userValidators");
const loginValidator = require("../validators/loginValidators");
// const validatorResetPassword = require("../validators/resetPassword")


router.get("/", allUsers);
router.get("/:id", userById);validatorCreateUser
router.post("/register", fileUpload.single("file"), validatorCreateUser, register);
router.delete("/:id", userDelete);
router.patch("/:id",  fileUpload.single("file"), validatorCreateUser, upUser);
router.post("/login", loginValidator ,loginUser)


router.post("/forgot", forgot);
router.get("/reset/:token", reset);
router.post("/resetpassword/:token", saveNewPassword);

module.exports = router;



