const nodemailer = require("nodemailer");
const { createNewUsers,
        login, 
        updateUser,
        changePasswordById
       } = require("./userModelos.js");
const {
  checkPassword,
  hashPassword,
} = require("../util/handlerPassword.js");
require("dotenv").config();
const { tokenSign, tokenVerify } = require("../util/handlerToken.js");
const UserDto = require("../DTO/userDto.js");


// REGISTER --------------------------------------------------

const register = async (req, res, next) => {
  const image = `${process.env.public_url}/${req.file.filename}`;
  const password = await hashPassword(req.body.password);
  const dbResponse = await createNewUsers({ ...req.body, password, image });
  if (dbResponse instanceof Error) return next(dbResponse);

  const user = {
    name: req.body.name,
    email: req.body.email,
  };

  const tokenData = {
    token: await tokenSign(user),
    user: user,
  };
  res.status(200).json({
    message: `El usuario ${req.body.name} fue creado !!!`,
    token_info: tokenData,
  });
};

const public_url = process.env.public_url;

// MAIL TRAP CONFIGURACIÓN ------------------------------

var transport = nodemailer.createTransport({
  host: "sandbox.smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "a380307dd0e1f4",
    pass: "77895a751e15fa"
  }
});

// FORGOT ------------------------------------------------

const forgot = async (req, res, next) => {
  const dbResponse = await login(req.body.email);
  
  if (!dbResponse.length) return next();

  const user = {
    id: dbResponse[0].id,
    name: dbResponse[0].name,
    email: dbResponse[0].email,
  };

  const token = await tokenSign(user, "1h");
  const link = `${public_url}/user/${token}`;
  console.log(link)
 
  let mailDetails = {
    from: "galera.eduardo@gmail.com",
    to: user.email,
    subject: "Password recovery with magic link",
    html: `<h2> To reset your password, please click on the link and follow instructions</h2>
      <a href= ${link}>Click recover your password</a>`,
  };

  transport.sendMail(mailDetails, (error, data) => {
    if (error) {
      error.message = "Internal server error";
      return next(error);
    } else {
      res.status(200).json({
        message: `Hi ${user.name} we ve sent email with instructions to ${user.email} hurry up....!!!`,
      });
    }
  });
};

// LOGIN --------------------------------------------------------

const loginUser = async (req, res, next) => {
  const dbResponse = await login(req.body.email);
  const passwordMatch = await checkPassword(
    req.body.password,
    dbResponse[0].password
  );
  // console.log(passwordMatch)
  // console.log(passwordMatch)
  if (passwordMatch) {
    const user = {
      id: dbResponse[0].id,
      name: dbResponse[0].name,
      email: dbResponse[0].email,
    };

    tokenData = {
      token: await tokenSign(user),
      user: user,
    };

    res
      .status(200)
      .json({ message: `User ${user.name} autorizado`, jwt: tokenData });
  } else {
    let error = new Error("Unauthorized");
    error.status = 401;
    next();
  }
};

const reset = async (req, res, next) => {
  const { token } = req.params
  const tokenStatus = await tokenVerify(token)
  if (tokenStatus instanceof Error) {
      res.status(403).json({ message: "Token expired" })
  } else {
      res.render("reset", { tokenStatus, token })
    }
}


// SAVE NEW PASSWORD --------------------------------------

const saveNewPassword = async (req, res, next) => {
    const token = req.params.token;
    // console.log(token,"1")
    const tokenStatus = await tokenVerify(token);
    
    if(tokenStatus instanceof Error) return res.send(tokenStatus);
    // console.log(tokenStatus,"2")
    const newPassword = await hashPassword(req.body.password_1);
    // console.log(req.body.password_1, "3")
    // console.log(tokenStatus.id, "4")
    // console.log(newPassword,"5")

    const dbResponse = await changePasswordById(tokenStatus.id, newPassword);
    console.log(dbResponse)
    dbResponse instanceof Error 
    ? next(dbResponse)
    : res.status(200).json({message: `Password change por user: ${tokenStatus.name}`}) 
};



module.exports = {
  register,
  loginUser,
  forgot,
  reset,
  saveNewPassword
};

