const notNumber = require("../util/isNaN");
const {
  getAllUsers,
  getUserById,
  updateUser,
  deleteUser,
  } = require("./userModelos");
require("dotenv").config();
const logger = require("../logs/userLogger")




const allUsers = async (req, res, next) => {
  const dbResponse = await getAllUsers();
  logger.log("silly", `${new Date().toLocaleString("es-AR")} users Ok`)
  if (dbResponse instanceof Error) return next(error);
  dbResponse.length ? res.status(200).json(dbResponse) : next();
};

const userById = async (req, res, next) => {
  notNumber(+req.params.id, next);
  const dbResponse = await getUserById(req.params.id);
  req.logger.info(JSON.stringify(dbResponse))
  if (dbResponse instanceof Error) return next(error);
  dbResponse.length ? res.status(200).json(dbResponse) : next();
};


const upUser = async (req, res, next) => {
  notNumber(+req.params.id, next);
  const image = `${process.env.public_url}/${req.file.filename}`;
  const password = await hashPassword(req.body.password);
  const dbResponse = await updateUser(+req.params.id, {
    ...req.body,
    password,
    image,
  });
  if (dbResponse instanceof Error) return next(dbResponse);
  dbResponse.affectedRows ? res.status(200).json(req.body) : next();
};

const userDelete = async (req, res, next) => {
  const dbResponse = await deleteUser(+req.params.id);
  if (dbResponse instanceof Error) return next(error);
  dbResponse.affectedRows
    ? res
        .status(200)
        .json({ message: `User eliminado con Id: ${req.params.id}` })
    : next();
};


module.exports = {
  allUsers,
  userById,
  upUser,
  userDelete,
};
