
 class UserDto{
    constructor(token){
        this.id = token.id
        this.name = token.name
        this.email = token.email
    }
}

module.exports =  UserDto