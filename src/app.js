require("dotenv").config();
const path = require("path")
const express = require("express");
const server = express();
const userRoutes = require("../src/user/userRouter");
const routerPosts = require("../src/posts/postsRouter");
const morgan = require("morgan");
const hbs = require("express-handlebars");
const cors = require("cors");
const attachLogger = require("../src/middleware/loggers")
const SwaggerUiExpress = require("swagger-ui-express");
const swaggerJSDoc = require("swagger-jsdoc");

// Conexión a Base de Datos
require("../db/db.config")

const PORT = process.env.PORT || 3030;

// creamos las opciones de documentación
const swaggerOptions ={
    definition:{
        openapi: "3.0.1",
        info:{
            title:"Documentación de mi API",
            description: "Documentación para Eduardo Galera"
        }
    },

    apis:[`${__dirname}/../docs/**/*.yaml`]
}

const specs = swaggerJSDoc(swaggerOptions)
// creo un midleware para swaggers
server.use("/docs", SwaggerUiExpress.serve, SwaggerUiExpress.setup(specs) )

// Middleware 
server.use(express.static("public"))
server.use(express.static("src/storage"))


// server.use(express.static("storage"))
server.use(morgan("dev"))
server.use(express.json())
server.use(express.urlencoded({extended: true }))
server.use(express.static("storage"))
server.use(cors())

//Handlebars
server.set("view engine", "hbs");
server.set("views", path.join(__dirname, "views") ); //path.join(__dirname, "views")
server.engine("hbs", hbs.engine({ extname: "hbs" }))

//Static Bootstrap
server.use("/css", express.static(path.join(__dirname, "node_modules/bootstrap/dist/css")))
server.use("/js", express.static(path.join(__dirname, "node_modules/bootstrap/dist/js")))

// loggers midleware 
server.use(attachLogger)

// Router
server.use("/user", userRoutes);
server.use("/posts", routerPosts);




server.listen(PORT, (err)=>{
    err 
    ? console.log(`Error: ${err}` )
    : console.log(`Conectados en : http://localhost:${PORT} ...`)
})

// 404 Todas las peticiones con 404 entran aca (CATCH ALL ROUTE)
server.use((req, res, next)=>{
    let error = new Error("Resource not found");
    error.status = 404;
    next(error)
 });
 
 // Error handler -------------------------------------------------
 server.use((error, req,res, next)=>{
     if(!error.status){
         error.status = 500;
     }
     res.status(error.status).json({status: error.status, message: error.message})
 });
 

